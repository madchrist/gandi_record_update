gandi\_record\_update
=====================

Create or update a record in your Gandi DNS zone, using the Gandi LiveDNS API

Requirements
------------
* Perl 5+
* curl
* A [Gandi LiveDNS API KEY](https://doc.livedns.gandi.net/)
* A domain configured to use the LiveDNS servers

Usage
-----

    Usage: gandi_record_update <apikey> <domain> <name> <type> <value> [<value>]... <ttl>       Create or update a DNS record
      or:  gandi_record_update [-h|--help]                                                      This message
    
      <apikey>   Your key to the Gandi LiveDNS API
      <domain>   The domain name, e.g.: mydomain.com
      <name>     The name of the record, if it doesn't exist it will be created, if it already exists it will be updated
      <type>     The record's type: A, CNAME, etc
      <value>    The record's value, there can be more than one value. If "myipv4" or "myipv6" is given, https://api64.ipify.org will be used to figure out your public IP
      <ttl>      The record's time to live, in seconds, e.g.: 600
    
    Examples:
      gandi_record_update 3z1fXG4A5z3g43R5e4g354B1 mydomain.com home A myipv4 600
      gandi_record_update 3z1fXG4A5z3g43R5e4g354B1 example.org orange CNAME black 3600
      gandi_record_update 3z1fXG4A5z3g43R5e4g354B1 test.net www A 10.0.0.1 10.0.0.2 10.0.0.3 300
      gandi_record_update 3z1fXG4A5z3g43R5e4g354B1 ipv6forlife.org www AAAA 2001:789:42::1 myipv6 300
